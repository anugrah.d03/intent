package com.anugrahdeagung10191012.praktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
    }
}